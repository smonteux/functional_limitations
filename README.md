**Carbon and nitrogen cycling in Yedoma permafrost controlled by microbial functional limitations** 

Sylvain Monteux, Frida Keuper, Sebastien Fontaine, Konstantin Gavazov, Sara Hallin, Jaanis Juhanson, Eveline J. Krab, Sandrine Revaillot, Erik Verbruggen, Josefine Walz, James T. Weedon, Ellen Dorrepaal

*Nature Geoscience* **2020**, [https://www.nature.com/articles/s41561-020-00662-4](https://www.nature.com/articles/s41561-020-00662-4)

Data and figure-production script (Downstream/ folder) are also found on figshare at [https://doi.org/10.6084/m9.figshare.7713308](https://doi.org/10.6084/m9.figshare.7713308)

Sequencing data is deposited at ENA under accession number PRJEB29467 (ERP111771).

### Structure ###

* 16S/ stores:
    * the bacterial bioinformatics pipeline ***MonteuxKeuper_16S.ipynb*** (Jupyter notebook)
    * an HTML render of this Jupyter notebook ***MonteuxKeuper_16S.html*** 
    * the bacterial mapping file ***MonteuxKeuper_Mapping.txt***
    * additional scripts and convenience functions in the 16S/scripts/ folder
* ITS/ stores:
    * the fungal bioinformatics pipeline ***MonteuxKeuper_ITS.ipynb*** (Jupyter notebook)
    * an HTML render of this Jupyter notebook ***MonteuxKeuper_ITS.html*** 
    * the fungal mapping file ***MonteuxKeuper_map_ITS.txt*** as well as ***MonteuxKeuper_map_ITS_alpharar.txt*** 
    * additional scripts and convenience functions in the ITS/scripts/ folder
* reads/ is destined to welcome the downloaded reads (.fastq files) from ENA
* Downstream/ stores the data files and script used to produce the figures found in the article
    * the figure-generating script ***MonteuxKeuper_Functional_limitations.R***
    * the bioinformatics-derived data used in the article in the Downstream/R/ folder
    * the other data used in the article in the Downstream/Metadata/ folder
    * (if the 16S and/or ITS bioinformatics pipeline were run): the fresh bioinformatics-derived data in the Downstream/Rnew/ folder
    * (if the figure-generating script was run): the figure panels in the /Downstream/Figures/ folder, and the Source Data in the /Downstream/SourceData/ folder
* **environment.yml** defines the conda environment used throughout the 16S and ITS pipelines 
* **Note:** Downloading of both bacterial and fungal raw sequence files, as well as setting up the conda environment, take place as the first steps of the ***MonteuxKeuper_16S.ipynb*** notebook, the first section of which should therefore be run prior to the ITS pipeline.

### Conda environment ###
In this pipeline, qiime is installed on a conda env called "qiime", and is called by "conda activate qiime", feel free to replace that with whatever you call it, or to remove that line if qiime is installed without conda. You will also need to install Jupyter along with a bash kernel if not using the provided conda environment.

The conda environment can be created using the environment.yml file as follows

``` conda env create -n qiime -f environment.yml ```

This should install most packages, but some (e.g. RDP classifier) might still need to be manually installed

### Contact ###

* Corresponding authors: Sylvain Monteux [sylvain.monteux@slu.se](mailto:sylvain.monteux@slu.se), Frida Keuper [frida.keuper@inrae.fr](mailto:frida.keuper@inrae.fr)
* [On ResearchGate](https://www.researchgate.net/profile/Sylvain_Monteux)

