mkdir Yedoma_rarefied5000_tsv
for file in ./Yedoma_rar_5000/*
do
	file_name="$(basename "$file" | cut -d "." -f 1)"
	biom convert -i "$file" -o Yedoma_rarefied5000_tsv/"$file_name" --table-type "OTU table" --to-tsv
done
