mkdir inoc_rar5000_tsv
for file in ./inoc_rar5000/*
do
	file_name="$(basename "$file" | cut -d "." -f 1)"
	biom convert -i "$file" -o inoc_rar5000_tsv/"$file_name" --table-type "OTU table" --to-tsv
done
